﻿using uMod.Auth;
using uMod.Common;

namespace uMod.Game.SevenDaysToDie
{
    /// <summary>
    /// Represents a SevenDaysToDie player manager
    /// </summary>
    public class SevenDaysToDiePlayerManager : PlayerManager<SevenDaysToDiePlayer>
    {
        /// <summary>
        /// Create a new instance of the SevenDaysToDiePlayerManager class
        /// </summary>
        /// <param name="application"></param>
        /// <param name="logger"></param>
        public SevenDaysToDiePlayerManager(IApplication application, ILogger logger) : base(application, logger)
        {
        }

        /// <summary>
        /// Determine if specified key matches the specified player
        /// </summary>
        /// <param name="partialNameOrIdOrIp"></param>
        /// <param name="player"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        protected override bool IsPlayerKeyMatch(string partialNameOrIdOrIp, SevenDaysToDiePlayer player, int playerFilter = 0)
        {
            if (base.IsPlayerKeyMatch(partialNameOrIdOrIp, player, playerFilter))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Determine if specified key matches the specified player
        /// </summary>
        /// <param name="partialNameOrIdOrIp"></param>
        /// <param name="player"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        protected override bool IsPlayerKeyMatch(string partialNameOrIdOrIp, IPlayer player, int playerFilter = 0)
        {
            if (base.IsPlayerKeyMatch(partialNameOrIdOrIp, player, playerFilter))
            {
                return true;
            }

            return false;
        }
    }
}
