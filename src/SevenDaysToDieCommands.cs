using System.Collections.Generic;
using System.Linq;
using uMod.Command;
using uMod.Common;
using uMod.Common.Command;
using static SdtdConsole;

namespace uMod.Game.SevenDaysToDie
{
    /// <summary>
    /// Represents a binding to a generic command system
    /// </summary>
    public class SevenDaysToDieCommands : ICommandSystem
    {
        #region Initialization

        // The universal provider
        private readonly SevenDaysToDieProvider Universal = SevenDaysToDieProvider.Instance;

        // The console player
        internal readonly SevenDaysToDieConsolePlayer ConsolePlayer;

        // Command handler
        internal readonly ICommandHandler CommandHandler;

        // Default universal commands
        internal readonly Commands DefaultCommands;

        // All registered commands
        internal IDictionary<string, RegisteredCommand> RegisteredCommands;

        internal class NativeCommand : ConsoleCmdAbstract
        {
            /// <summary>
            /// The name of the command
            /// </summary>
            public string Command;

            /// <summary>
            /// The player that sent the command
            /// </summary>
            public IPlayer Sender;

            /// <summary>
            /// The callback
            /// </summary>
            public CommandCallback Callback;

            /// <summary>
            /// Executes the command
            /// </summary>
            /// <param name="args"></param>
            /// <param name="sender"></param>
            public override void Execute(List<string> args, CommandSenderInfo sender)
            {
                IPlayer player = sender.RemoteClientInfo?.IPlayer ?? Sender;
                Callback?.Invoke(player, Command, string.Join(" ", args), args.ToArray());
            }

            /// <summary>
            /// Gets the variant commands for the command
            /// </summary>
            /// <returns></returns>
            public override string[] GetCommands() => new[] { Command };

            /// <summary>
            /// Gets the description for the command
            /// </summary>
            /// <returns></returns>
            public override string GetDescription()
            {
                return "See plugin documentation for command description"; // TODO: Implement when possible and localize
            }

            /// <summary>
            /// Gets the help for the command
            /// </summary>
            /// <returns></returns>
            public override string GetHelp()
            {
                return "See plugin documentation for command help"; // TODO: Implement when possible and localize
            }
        }

        // Registered commands
        internal class RegisteredCommand
        {
            /// <summary>
            /// The plugin that handles the command
            /// </summary>
            public readonly IPlugin Source;

            /// <summary>
            /// The name of the command
            /// </summary>
            public readonly string Command;

            /// <summary>
            /// The callback
            /// </summary>
            public readonly CommandCallback Callback;

            /// <summary>
            /// The callback
            /// </summary>
            public IConsoleCommand OriginalCallback;

            /// <summary>
            /// The 7 Days to Die command
            /// </summary>
            public NativeCommand SevenDaysToDieCommand;

            /// <summary>
            /// The original command when overridden
            /// </summary>
            public IConsoleCommand OriginalSevenDaysToDieCommand;

            /// <summary>
            /// Initializes a new instance of the RegisteredCommand class
            /// </summary>
            /// <param name="source"></param>
            /// <param name="command"></param>
            /// <param name="callback"></param>
            public RegisteredCommand(IPlugin source, string command, CommandCallback callback)
            {
                Source = source;
                Command = command;
                Callback = callback;
            }
        }

        /// <summary>
        /// Initializes the command system
        /// </summary>
        /// <param name="commandHandler"></param>
        public SevenDaysToDieCommands(ICommandHandler commandHandler)
        {
            RegisteredCommands = new Dictionary<string, RegisteredCommand>();
            CommandHandler = commandHandler;
            CommandHandler.Callback = CommandCallback;
            CommandHandler.CommandFilter = RegisteredCommands.ContainsKey;
            ConsolePlayer = new SevenDaysToDieConsolePlayer();
            DefaultCommands = new Commands();
        }

        public CommandState CommandCallback(IPlayer caller, string command, string fullCommand, object[] context = null, ICommandInfo commandInfo = null)
        {
            if (!RegisteredCommands.TryGetValue(command, out RegisteredCommand registeredCommand))
            {
                return CommandState.Unrecognized;
            }

            return registeredCommand.Callback(caller, command, fullCommand, context, commandInfo);
        }

        #endregion Initialization

        #region Command Registration

        /// <summary>
        /// Registers the specified command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="plugin"></param>
        /// <param name="callback"></param>
        public void RegisterCommand(string command, IPlugin plugin, CommandCallback callback)
        {
            // Convert command to lowercase and remove whitespace
            command = command.ToLowerInvariant().Trim();

            // Split the command
            string[] split = command.Split('.');
            string parent = split.Length >= 2 ? split[0].Trim() : "global";
            string name = split.Length >= 2 ? string.Join(".", split.Skip(1).ToArray()) : split[0].Trim();
            if (parent == "global")
            {
                command = name;
            }

            // Check if the command can be overridden
            if (!CanOverrideCommand(command))
            {
                throw new CommandAlreadyExistsException(command);
            }

            // Set up a new command
            RegisteredCommand newCommand = new RegisteredCommand(plugin, command, callback)
            {
                // Create a new native command
                SevenDaysToDieCommand = new NativeCommand
                {
                    Command = command,
                    Callback = callback,
                    Sender = ConsolePlayer
                }
            };

            // Check if the command already exists in another plugin
            if (RegisteredCommands.TryGetValue(command, out RegisteredCommand cmd))
            {
                if (newCommand.SevenDaysToDieCommand == cmd.SevenDaysToDieCommand)
                {
                    newCommand.OriginalSevenDaysToDieCommand = cmd.SevenDaysToDieCommand;
                }

                string newPluginName = plugin?.Name ?? "An unknown plugin"; // TODO: Localization
                string previousPluginName = cmd.Source?.Name ?? "an unknown plugin"; // TODO: Localization
                Interface.uMod.LogWarning($"{newPluginName} has replaced the '{command}' command previously registered by {previousPluginName}"); // TODO: Localization
            }

            // Check if the command already exists as a native command
            IConsoleCommand nativeCommand = SdtdConsole.Instance.GetCommand(command);
            if (nativeCommand != null)
            {
                if (newCommand.OriginalCallback == null)
                {
                    newCommand.OriginalCallback = nativeCommand;
                }

                if (SdtdConsole.Instance.m_Commands.Contains(nativeCommand))
                {
                    SdtdConsole.Instance.m_Commands.Remove(nativeCommand);
                    foreach (string nCommand in newCommand.SevenDaysToDieCommand.GetCommands())
                    {
                        SdtdConsole.Instance.m_CommandsAllVariants.Remove(nCommand);
                    }
                }

                string newPluginName = plugin?.Name ?? "An unknown plugin"; // TODO: Localization
                Interface.uMod.LogWarning($"{newPluginName} has replaced the '{command}' command previously registered by {Universal.GameName}"); // TODO: Localization
            }

            // Register the command
            RegisteredCommands.Add(command, newCommand);
            if (!SdtdConsole.Instance.m_Commands.Contains(newCommand.SevenDaysToDieCommand))
            {
                SdtdConsole.Instance.m_Commands.Add(newCommand.SevenDaysToDieCommand);
            }
            foreach (string nCommand in newCommand.SevenDaysToDieCommand.GetCommands())
            {
                if (!string.IsNullOrEmpty(nCommand))
                {
                    SdtdConsole.Instance.m_CommandsAllVariants.Add(nCommand, newCommand.SevenDaysToDieCommand);
                }
            }

            // Register the command permission
            /*string[] variantCommands = newCommand.SevenDaysToDieCommand.GetCommands();
            if (!GameManager.Instance.adminTools.IsPermissionDefinedForCommand(variantCommands) && newCommand.SevenDaysToDieCommand.DefaultPermissionLevel != 0)
            {
                GameManager.Instance.adminTools.AddCommandPermission(command, newCommand.SevenDaysToDieCommand.DefaultPermissionLevel, false);
            }*/
        }

        #endregion Command Registration

        #region Command Unregistration

        /// <summary>
        /// Unregisters the specified command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="plugin"></param>
        public void UnregisterCommand(string command, IPlugin plugin)
        {
            IConsoleCommand cmd = SdtdConsole.Instance.GetCommand(command);
            if (SdtdConsole.Instance.m_Commands.Contains(cmd))
            {
                SdtdConsole.Instance.m_Commands.Remove(cmd);
                foreach (string nCommand in cmd.GetCommands())
                {
                    SdtdConsole.Instance.m_CommandsAllVariants.Remove(nCommand);
                }
            }

            // Remove the command
            RegisteredCommands.Remove(command);
        }

        #endregion Command Unregistration

        #region Command Handling

        /// <summary>
        /// Checks if a command can be overridden
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        private bool CanOverrideCommand(string command)
        {
            if (!RegisteredCommands.TryGetValue(command, out RegisteredCommand cmd) || !cmd.Source.IsCorePlugin)
            {
                string[] split = command.Split('.');
                string parent = split.Length >= 2 ? split[0].Trim() : "global";
                string name = split.Length >= 2 ? string.Join(".", split.Skip(1).ToArray()) : split[0].Trim();
                return !SevenDaysExtension.RestrictedCommands.Contains(command) && !SevenDaysExtension.RestrictedCommands.Contains($"{parent}.{name}");
            }

            return true;
        }

        /// <summary>
        /// Handles a chat command message
        /// </summary>
        /// <param name="player"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public CommandState HandleChatMessage(IPlayer player, string message) => CommandHandler.HandleChatMessage(player, message);

        #endregion Command Handling
    }
}
