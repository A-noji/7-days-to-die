﻿using System;
using uMod.Common;
using uMod.Plugins;

namespace uMod.Game.SevenDaysToDie
{
    /// <summary>
    /// Responsible for loading the core plugin
    /// </summary>
    public class SevenDaysToDiePluginLoader : PluginLoader
    {
        public override Type[] CorePlugins => new[] { typeof(SevenDaysToDie) };

        public SevenDaysToDiePluginLoader(ILogger logger) : base(logger)
        {
        }
    }
}
