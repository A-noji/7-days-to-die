using Platform.Steam;
using System;
using System.Globalization;
using uMod.Auth;
using uMod.Common;
using uMod.Text;
using uMod.Unity;
using UnityEngine;

namespace uMod.Game.SevenDaysToDie
{
    /// <summary>
    /// Represents a player, either connected or not
    /// </summary>
    public class SevenDaysToDiePlayer : UniversalPlayer, IPlayer
    {
        #region Initialization

        internal ClientInfo clientInfo;
        internal EntityPlayer entityPlayer;
        internal PlatformUserIdentifierAbs identifier;

        public SevenDaysToDiePlayer(string playerId, string playerName)
        {
            // Store player details
            Id = playerId;
            Name = playerName.Sanitize();
        }

        public SevenDaysToDiePlayer(ClientInfo clientInfo) : this(((UserIdentifierSteam)clientInfo.PlatformId).ReadablePlatformUserIdentifier, clientInfo.playerName)
        {
            // Store player objects
            this.clientInfo = clientInfo;
            this.entityPlayer = GameManager.Instance.World.Players.dict[clientInfo.entityId];
            this.identifier = clientInfo.InternalId;
        }

        #endregion Initialization

        #region Objects

        /// <summary>
        /// Gets the object that backs the player
        /// </summary>
        public object Object { get; set; }

        /// <summary>
        /// Gets the player's last command type
        /// </summary>
        public CommandType LastCommand { get; set; }

        /// <summary>
        /// Reconnects the gamePlayer to the player object
        /// </summary>
        /// <param name="gamePlayer"></param>
        public void Reconnect(object gamePlayer)
        {
            // Reconnect player objects
            Object = gamePlayer;
            clientInfo = gamePlayer as ClientInfo;
        }

        #endregion Objects

        #region Information

        /// <summary>
        /// Gets/sets the name for the player
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the ID for the player (unique within the current game)
        /// </summary>
        public override string Id { get; }

        /// <summary>
        /// Gets the language for the player
        /// </summary>
        public CultureInfo Language => throw new NotImplementedException(); // TODO: Implement when possible

        /// <summary>
        /// Gets the IP address for the player
        /// </summary>
        public string Address => clientInfo?.ip ?? string.Empty;

        /// <summary>
        /// Gets the average network ping for the player
        /// </summary>
        public int Ping => clientInfo?.ping ?? 0;

        /// <summary>
        /// Gets if the player is a server admin
        /// </summary>
        public override bool IsAdmin => clientInfo != null && GameManager.Instance.adminTools.IsAdmin(clientInfo)
            || BelongsToGroup(Interface.uMod.Auth.Configuration.Groups.Administrators);

        /// <summary>
        /// Gets if the player is a server moderator
        /// </summary>
        public override bool IsModerator => IsAdmin || BelongsToGroup(Interface.uMod.Auth.Configuration.Groups.Moderators);

        /// <summary>
        /// Gets if the player is banned
        /// </summary>
        public bool IsBanned => GameManager.Instance.adminTools.IsBanned(identifier, out _, out _);

        /// <summary>
        /// Gets if the player is connected
        /// </summary>
        public bool IsConnected => clientInfo.litenetPeerConnectId >= 0L;

        /// <summary>
        /// Gets if the player is alive
        /// </summary>
        public bool IsAlive => !IsDead;

        /// <summary>
        /// Gets if the player is dead
        /// </summary>
        public bool IsDead
        {
            get
            {
                EntityPlayer entityPlayer = clientInfo != null ? GameManager.Instance.World.Players.dict[clientInfo.entityId] : null;
                return entityPlayer != null && entityPlayer.bDead;
            }
        }

        /// <summary>
        /// Gets if the player is sleeping
        /// </summary>
        public bool IsSleeping
        {
            get
            {
                return entityPlayer != null && entityPlayer.IsSleeping;
            }
        }

        /// <summary>
        /// Gets if the player is the server
        /// </summary>
        public bool IsServer => false;

        /// <summary>
        /// Gets/sets if the player has connected before
        /// </summary>
        public bool IsReturningPlayer { get; set; }

        /// <summary>
        /// Gets/sets if the player has spawned before
        /// </summary>
        public bool HasSpawnedBefore { get; set; }

        #endregion Information

        #region Administration

        /// <summary>
        /// Bans the player for the specified reason and duration
        /// </summary>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        public void Ban(string reason, TimeSpan duration = default)
        {
            // Check if already banned
            if (!IsBanned)
            {
                // Ban player with reason
                GameManager.Instance.adminTools.AddBan(clientInfo.playerName, identifier, new DateTime(duration.Ticks), reason);

                // Kick player with reason
                Kick(reason);
            }
        }

        /// <summary>
        /// Gets the amount of time remaining on the player's ban
        /// </summary>
        public TimeSpan BanTimeRemaining
        {
            get
            {
                if (GameManager.Instance.adminTools.bannedUsers.ContainsKey(identifier))
                {
                    AdminToolsClientInfo clientInfo = GameManager.Instance.adminTools.bannedUsers[identifier];
                    return clientInfo.BannedUntil.TimeOfDay;
                }

                return TimeSpan.Zero;
            }
        }

        /// <summary>
        /// Kicks the player from the game
        /// </summary>
        /// <param name="reason"></param>
        public void Kick(string reason = "")
        {
            if (IsConnected)
            {
                GameUtils.KickPlayerData kickData = new GameUtils.KickPlayerData(GameUtils.EKickReason.ManualKick, 0, default, reason);
                GameUtils.KickPlayerForClientInfo(clientInfo, kickData);
            }
        }

        /// <summary>
        /// Unbans the player
        /// </summary>
        public void Unban()
        {
            // Check if already unbanned
            if (IsBanned)
            {
                // Unban player
                GameManager.Instance.adminTools.RemoveBan(identifier);
            }
        }

        #endregion Administration

        #region Character

        /// <summary>
        /// Gets/sets the player's health
        /// </summary>
        public float Health
        {
            get
            {
                return entityPlayer?.Health ?? 0f;
            }
            set
            {
                if (entityPlayer != null && entityPlayer.Stats != null)
                {
                    entityPlayer.Stats.Health.Value = value;
                }
            }
        }

        /// <summary>
        /// Gets/sets the player's maximum health
        /// </summary>
        public float MaxHealth
        {
            get
            {
                return entityPlayer?.GetMaxHealth() ?? 0f;
            }
            set
            {
                if (entityPlayer != null && entityPlayer.Stats != null)
                {
                    entityPlayer.Stats.Health.BaseMax = value;
                }
            }
        }

        /// <summary>
        /// Heals the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Heal(float amount)
        {
            entityPlayer?.AddHealth((int)amount);
        }

        /// <summary>
        /// Damages the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Hurt(float amount)
        {
            entityPlayer?.DamageEntity(new DamageSource(EnumDamageSource.External, EnumDamageTypes.None), (int)amount, false);
        }

        /// <summary>
        /// Causes the player's character to die
        /// </summary>
        public void Kill()
        {
            entityPlayer?.Kill(DamageResponse.New(new DamageSource(EnumDamageSource.External, EnumDamageTypes.None), true));
        }

        /// <summary>
        /// Renames the player to specified name
        /// </summary>
        /// <param name="newName"></param>
        public void Rename(string newName)
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        /// <summary>
        /// Resets the player's character stats
        /// </summary>
        public void Reset()
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        /// <summary>
        /// Respawns the player's character
        /// </summary>
        public void Respawn()
        {
            // Check if unbanned already
            if (IsBanned)
            {
                // Set to unbanned
                GameManager.Instance.adminTools.RemoveBan(identifier);
            }
        }

        /// <summary>
        /// Respawns the player's character at specified position
        /// </summary>
        public void Respawn(Position pos)
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        #endregion Character

        #region Positional

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <returns></returns>
        public Position Position()
        {
            return (entityPlayer?.transform?.position ?? Vector3.zero).ToPosition();
        }

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Position(out float x, out float y, out float z)
        {
            Position pos = Position();
            x = pos.X;
            y = pos.Y;
            z = pos.Z;
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Teleport(float x, float y, float z)
        {
            if (IsConnected && clientInfo != null)
            {
                NetPackageTeleportPlayer netPackageTeleportPlayer = NetPackageManager.GetPackage<NetPackageTeleportPlayer>().Setup(new Vector3(x, y, z));
                clientInfo.SendPackage(netPackageTeleportPlayer);
            }
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="pos"></param>
        public void Teleport(Position pos) => Teleport(pos.X, pos.Y, pos.Z);

        #endregion Positional

        #region Chat and Commands

        /// <summary>
        /// Sends the specified message and prefix to the player
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Message(string message, string prefix, params object[] args)
        {
            if (!string.IsNullOrEmpty(message) && IsConnected)
            {
                message = args.Length > 0 ? string.Format(Formatter.ToRoKAnd7DTD(message), args) : Formatter.ToRoKAnd7DTD(message);
                clientInfo.SendPackage(NetPackageManager.GetPackage<NetPackageChat>().Setup(EChatType.Global, clientInfo.entityId, prefix != null ? $"{prefix} {message}" : message, null, false, null));
            }
        }

        /// <summary>
        /// Sends the specified message to the player
        /// </summary>
        /// <param name="message"></param>
        public void Message(string message) => Message(message, null);

        /// <summary>
        /// Replies to the player with the specified message and prefix
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Reply(string message, string prefix, params object[] args) => Message(message, prefix, args);

        /// <summary>
        /// Replies to the player with the specified message
        /// </summary>
        /// <param name="message"></param>
        public void Reply(string message) => Message(message, null);

        /// <summary>
        /// Runs the specified console command on the player
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        public void Command(string command, params object[] args)
        {
            if (!string.IsNullOrEmpty(command) && IsConnected)
            {
                SdtdConsole.Instance.ExecuteSync($"{command} {string.Join(" ", Array.ConvertAll(args, x => x.ToString()))}", clientInfo);
            }
        }

        #endregion Chat and Commands
    }
}
